const production = ( process.env.NODE_ENV == 'production' );

//base part
let gulp = require('gulp'),
    rename  = require('gulp-rename'),
    sourcemaps = require('gulp-sourcemaps'),
    nothing = require("gulp-empty"),
    shell = require('gulp-shell');

//css part
let sass = require('gulp-sass'),
    cleanCSS = require('gulp-clean-css'),
    autoprefixer = require('gulp-autoprefixer');

const buildFolder = 'static';

let pathFiles = {
    build : {
        css  : './' + buildFolder + '/css/'
    },
    src : {
        css  : './src/scss/**/*.scss'
    }
};

function swallowError(error){
    console.log(error.toString());
    this.emit('end');
}

gulp.task('styles', function() {
    return gulp.src('./src/scss/main.scss')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .on('error', swallowError)
        .pipe(autoprefixer({
            browsers: ['last 10 versions', '> 5%'],
            cascade: false
        }))
        .pipe(cleanCSS())
        .pipe(rename('style.min.css'))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(pathFiles.build.css));
});

gulp.task('gulp_watch', function () {
    gulp.watch(pathFiles.src.css, gulp.series('styles'));
});

gulp.task('build', gulp.series('styles'));

let tasks = gulp.series('build', 'gulp_watch');

if(production){
    tasks = gulp.series('build');
}

gulp.task('default', tasks);