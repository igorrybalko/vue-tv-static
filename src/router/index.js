import Vue from 'vue';
import Router from 'vue-router';

import HomePage from '../components/pages/HomePage';

//get dinamic page
const RadioPage = () => import('../components/pages/RadioPage');

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'HomePage',
      component: HomePage
    },
    {
      path: '/radio',
      name: 'RadioPage',
      component: RadioPage
    }
  ]
})